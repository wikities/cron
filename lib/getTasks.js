const client = require('./graphql');

const query = `query MyQuery {
    scripts {
      active
      cron
      handler
      name
      state
    }
  }
  `;

module.exports.getTasks = async () => {
  try {
    const scripts = (await client.request(query)).scripts;
    return scripts;
  } catch (error) {
    console.log('[e] GraphQL connection error');
    return [];
  }

}
