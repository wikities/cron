const client = require('./graphql');

const query = `mutation MyMutation($objects: [scripts_insert_input!]!) {
    insert_scripts(objects: $objects, on_conflict: {constraint: scripts_pkey, update_columns: [handler,cron,active]}) {
      affected_rows
    }
  }
  `;

module.exports.exportTask = async (object) => { return await client.request(query, { objects: [object] }) };
