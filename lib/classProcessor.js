const cron = require('node-cron');
const chalk = require('chalk');
const normalize = require('normalize-path');

module.exports = class classProcessor {

    constructor({ scriptsPath, storagePath }) {
        this.tasks = {};
        this.executions = {};
        this.ctx = {};
        this.scriptsPath = normalize(scriptsPath);
        this.storagePath = normalize(storagePath);
        console.log(`[x] Init scriptsPath [${this.scriptsPath}]`);
        console.log(`[x] Init storagePath [${this.storagePath}]`);
    }

    async importTasks() {
        const tasksPath = normalize(this.scriptsPath + '/tasks.js');
        this.log(`[x] Open scripts [${tasksPath}]`);
        let localTasks = require(tasksPath);
        for (const taskname in localTasks) {
            localTasks[taskname] = this.#normalizeTask({
                name: taskname,
                task: localTasks[taskname],
                remote: false
            });
        };
        this.tasks = localTasks;
        this.log(`[x] Tasks founded: ${Object.keys(this.tasks).length}`);
    }

    #normalizeTask({ task, remote = true, name }) {
        let res = { ...task, name };
        if (task.handler) res.handler = eval('(' + task.handler + ')');
        task.remote = task.remote || remote;
        task.active = task.active || false;
        return res;
    }

    get activeTasks() {
        let res = {}
        Object.keys(this.tasks).forEach(name => {
            if (this.tasks[name].active) res[name] = this.tasks[name];
        });
        return res;
    }

    async #executeTask(task, ctx) {
        try {
            if (task.handler) {
                return await task.handler(ctx);
            } else if (task.name) {
                const scriptPath = normalize(this.scriptsPath + '/' + task.name);
                const handler = require(scriptPath).handler;
                return await handler(ctx);
            } else {
                throw ('No task path or handler', task)
            }
        } catch (error) {
            console.log("[error] Can't execute task", task, error);
        }
    }

    #startCron(croncode, handler) {
        cron.schedule(croncode, handler);
    }

    #isCronTask(task) {
        return !!task.cron;
    }
    #isCanStartTask(taskname) {
        return !this.executions[taskname];
    }

    async initCron() {
        const that = this;
        const tasks = this.activeTasks;
        this.log('[w] Init cron...', tasks);
        Object.keys(tasks).forEach(async taskname => {
            const task = tasks[taskname];
            this.log(`[x] Task "${taskname}" cron "${task.cron || 'none'}"`);

            if (this.#isCronTask(task)) {
                this.#startCron(task.cron, async () => {
                    if (this.#isCanStartTask(taskname)) {
                        that.log(`[cron] "${taskname}" cron ${chalk.bgGray(' ' + task.cron + ' ')} ${this.#isCanStartTask(taskname) ? chalk.green('execute') : chalk.yellow('skipped')}`);
                    };
                    if (this.#isCanStartTask(taskname)) {
                        that.executions[taskname] = new Date();
                        try {
                            await that.#executeTask(task, that);
                        } catch (error) {
                            this.log(`[e] Execution error`, error);
                        }
                        that.executions[taskname] = null;
                    } else {
                    }
                });
            } else if (task.one) {
                try {
                    await that.#executeTask(task, that.ctx);
                } catch (error) {
                    this.log(`[e] Execution error`, error);
                }
            }
        });
    }

}
