const webScraper = require('arachnida');

module.exports = async (url, html, expression) => {
    return new Promise(function (resolve, reject) {
        webScraper({
            url,
            html,
            data: expression,
        }, (err, json) => {
            if (err) {
                console.log('[error] Arachnida found nothing');
                resolve();
            } else {
                resolve(json);
            };
        });
    })
};