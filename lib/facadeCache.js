const cf = require('npm-cache-filename');
const fs = require('fs');
const normalize = require('normalize-path');
const { URL } = require('url');

/*
    Add read, clear method
*/

class cacheClass {
    constructor({ storagePath }) {
        this.storagePath = storagePath;
        console.log('[x] storagePath', storagePath);
    }

    clear(url) {
        try {
            const { dirpath, filepath } = this.#getPaths({ url, storagePath: this.storagePath });
            console.log('[cache] Clear', url);
            rmDir(dirpath);
            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    saveHtmlByUrl({ url, storagePath = this.storagePath, prefix = '', data }) {
        try {
            const { dirpath, filepath } = this.#getPaths({ url, storagePath, prefix });
            fs.mkdirSync(dirpath, { recursive: true });
            fs.writeFileSync(filepath, data);
            //   console.log('[SAVED]', filepath);

        } catch (error) {
            console.log('[error] Save HTML by URL error.', error);
        }
    }

    saveDomainByUrl(url, data) {
        const { origin } = new URL(url);
        const dirpath = normalize(cf(this.storagePath + '/cookies/', origin));
        const filepath = normalize(dirpath + '/domain.dat');
        fs.mkdirSync(dirpath, { recursive: true });
        fs.writeFileSync(filepath, data);
    }

    readDomainByUrl(url) {
        try {
            const { origin } = new URL(url);
            const dirpath = normalize(cf(this.storagePath + '/cookies/', origin));
            const filepath = normalize(dirpath + '/domain.dat');
            return fs.readFileSync(filepath).toString();

        } catch (error) {
            return;
        }
    }

    #getPaths({ url, storagePath, prefix }) {
        const dirpath = normalize(cf(storagePath + (prefix ? `/${prefix}/` : ''), url));
        const filepath = normalize(dirpath + '/index.html');
        return { dirpath, filepath };
    }

    async read({ url, callback, storagePath = this.storagePath, clear = false, prefix = '' }) {

        const { dirpath, filepath } = this.#getPaths({ url, storagePath, prefix });
        //        console.log(dirpath);
        if (isFileExist(filepath)) {
            console.log('[cached]', dirpath);
            return { path: filepath, data: fs.readFileSync(filepath).toString(), error: false };
        } else {
            try {
                fs.mkdirSync(dirpath, { recursive: true });
                const { data, error } = await callback(url);
                if (!error) {
                    fs.writeFileSync(filepath, data);
                    return { path: filepath, data, error };
                } else {
                    return { path: null, data: null, error };
                }
            } catch (error) {
                if (error) {
                    console.log('[error] Timeout')
                } else {
                    console.log('[cache error]', error);
                }
                return error
            }
        }
    }
}

function cutTxt(s) {
    return s.length > 40 ? s.slice(0, 40) : s;
}

function isFileExist(path) {
    try {
        return fs.existsSync(path)
    } catch (err) {
        return false;
    }
}

rmDir = function (dirPath) {
    try { var files = fs.readdirSync(dirPath); }
    catch (e) { return; }
    if (files.length > 0)
        for (var i = 0; i < files.length; i++) {
            var filePath = dirPath + '/' + files[i];
            if (fs.statSync(filePath).isFile())
                fs.unlinkSync(filePath);
            else
                rmDir(filePath);
        }
    fs.rmdirSync(dirPath);
};

module.exports = cacheClass;