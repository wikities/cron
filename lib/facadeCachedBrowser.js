const puppeteer = require('puppeteer');
const cacheClass = require('./facadeCache');
const devices = puppeteer.devices;
var URLToolkit = require('url-toolkit');
const normalize = require('normalize-path');
const fs = require('fs');
const { threadId } = require('worker_threads');

/*
    Virtual browser with locale cache
    Singleton pattern
*/

class cachedBrowserClass {

    constructor({ storagePath }) {
        this.loadingLimit = 3;
        this.loadingCounter = 1;
        this.puppeteer;
        this.cache = new cacheClass({ storagePath });
        this.options = {
            device: devices['iPad landscape'],
            mobile: false,
            images: false,
            css: false,
            cookie: false,
            headless: true,
            errorPhrase: null
        }
        this.prefix = '';
    }

    async screenShot({ url, path, fullpath }) {
        try {
            this.isImageEnabled = true;
            const page = await this.page();
            const result = await page.goto(url);
            if (result.status() === 200) {
                fs.mkdirSync(path, { recursive: true });
                await page.screenshot({ path: fullpath, type: 'jpeg' });
                return { error: false };
            } else {
                return { error: true };
            }
        } catch (error) {
            return { error: true };
        }
    }

    async clickAndCache(cssSelector, normalize, waitForSelector) {
        let page;
        try {
            this.loadingCounter++;
            page = await this.page();
            let url = await page.$eval(cssSelector, function (element) { return element.getAttribute('href'); });
            url = URLToolkit.buildAbsoluteURL(
                page.url(),
                url
            );
            url = await normalize(url);
            await page.click(cssSelector);
            await page.waitForSelector(waitForSelector);
            const html = await page.content();
            this.cache.saveHtmlByUrl({
                url,
                prefix: this.prefix,
                data: html
            });
            return { data: html, error: false, url };

        } catch (error) {
            return { error, data: await page.content() }
        }
    }

    #setOptions(options) {
        Object.assign(this.options, options);
        this.options.device = this.options.mobile ? devices['iPhone 6 landscape'] : devices['iPad landscape'];
        this.prefix = this.options.mobile ? 'mobile' : 'pc';
    }

    async download(url, options) {
        try {
            this.#setOptions(options);
            this.loadingCounter++;
            console.log(`[GET] [${this.prefix}]`, url);
            const res = await this.cache.read({
                url,
                prefix: this.prefix,
                callback: async (dwUrl) => {
                    const page = await this.page();
                    if (this.options.cookie) { await this.loadCookies(page, dwUrl); }
                    const response = await page.goto(dwUrl);
                    await this.saveCookies(page, dwUrl);
                    if (response.status() !== 200) {
                        console.log('[download] Error', response.status(), dwUrl);
                        return { error: response.status(), data: null };
                    } else {
                        const html = await page.content();
                        if (this.options.errorPhrase && html.indexOf(this.options.errorPhrase) > -1) {
                            return { error: true, data: null };
                        }
                        return { error: false, data: html };
                    }
                }
            });
            return res;

        } catch (error) {
            console.log(error);
            return { error: true };
        }
    }

    async browser(update = false) {
        if (update) {
            await this.puppeteer.close();
        }
        if (!this.puppeteer) {
            console.log('[RESTART] Browser');
            this.puppeteer = await this.#initBrowser()
        }
        return this.puppeteer;
    }

    async page() {
        try {
            let pages = await (await this.browser()).pages();
            if (pages.length > 0) {
                await this.#setPageOptions(pages[0]);
                return pages[0];
            } else {
                const page = await (await this.browser()).newPage();
                await this.#setPageOptions(page);
                return page;
            }
        } catch (error) {
            console.log('PAGE INIT ERROR', error);
            await this.browser(true);
            return await this.page()
        }
        // if (this.loadingCounter >= this.loadingLimit) {
        //     this.loadingCounter = 1;
        //     return await this.#reInitPage(true);
        // } else {
        //     return await this.#reInitPage();
        // }
    }


    async saveCookies(page, url) {
        this.cache.saveDomainByUrl(url, JSON.stringify(await page.cookies()));
    }

    async loadCookies(page, url) {
        const cookies = this.cache.readDomainByUrl(url);
        if (cookies) {
            const deserializedCookies = JSON.parse(this.cache.readDomainByUrl(url));
            await page.setCookie(...deserializedCookies)
        }
    }

    async #initBrowser() {
        const browser = await puppeteer.launch({
            headless: this.options.headless,
            args: [
                '--lang=en',
                '--disable-breakpad',
                "--no-sandbox",
                '--disable-gpu',
                '--disable-dev-shm-usage',
                '--disable-setuid-sandbox',
                '--no-first-run',
                '--no-zygote',
                '--single-process'
            ]
        });
        //        browser.on('disconnected', this.#initBrowser());
        return browser;
    }

    async #setPageOptions(page) {
        page.setDefaultNavigationTimeout(60000);
        await page.emulate(this.options.device);
        if (!this.options.images) {
            await page.removeAllListeners("request");
            await page.setRequestInterception(true);
            page.on('request', (request) => {
                if (request.resourceType() === 'image' || request.resourceType() === 'stylesheet' || request.resourceType() === 'font') {
                    request.abort();
                } else { request.continue(); }
            });
        }
    }
}

module.exports = async (entity) => {
    if (!entity.ctx) entity.ctx = {};
    entity.ctx.browser = new cachedBrowserClass({ storagePath: normalize(entity.storagePath) });
};