const chalk = require('chalk');

module.exports = (service) => {
    service.log = (...args) => {
        const dt = new Date();
        const date = dt.toLocaleDateString();
        const time = dt.toLocaleTimeString();
        console.log(chalk.grey(`${date} ${time}`), ...args)
    }
}


