const exportTask = require('./lib/exportTask').exportTask;
const serialize = require('serialize-javascript');

const args = process.argv.slice(2);
(async () => {
    console.log('Export', args);
    const script = require(`./scripts/${args[0]}.js`);
    const object = {
        name: args[0],
        handler: serialize(script.handler),
        cron: args[1] || script.cron,
        active: true
    };

    //  console.log('Record', object);
    console.log(args[0], await exportTask(object));
})();

